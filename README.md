# create a testenv stack
`testenv stack create nginx-ctrl-v4 --cloud aws --num-datapaths 5 --enable-nap true`

`export STACK_ID=<stack id from output of testenv command>`

# create Ansible ini file from testenv stack 
`./testenv2ini.sh ${STACK_ID}`

# run playbook to set up NMS 
`ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook upg-setup-playbook.yaml -i /tmp/inventory-${STACK_ID}.ini`

# manually upload cert and publish nginx config 
* log into UI 
* add cert - files/pem-test.pem 
* publish nginx.conf to all instances 

# run traffic 
`ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook run-traffic-playbook.yaml -i /tmp/inventory-${STACK_ID}.ini`

# for upgrade, download latest RC to NMS host 
`testenv usercmd run download-artifact --artifact-path https://nginxdevopssvcs.blob.core.windows.net/cylon-indigo-generic-dev/platform/release-2-10-0/platform-repo-844075576.tar.gz "${STACK_ID}" ctrl-1`

# manually upgrade NMS host 
ssh into ctrl-1, run yum or apt-get command as per: 

https://docs.nginx.com/nginx-management-suite/admin-guides/installation/upgrade-guide/

# then update all Agents using this playbook:
`ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook upgrade-agent-playbook.yaml -i /tmp/inventory-${STACK_ID}.ini`

# run traffic again 
`ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook run-traffic-playbook.yaml -i /tmp/inventory-${STACK_ID}.ini`
