#!/usr/bin/env bash

# create stack.ini file for Ansible
# based on testenv stack ID
# parse the host and datapath IPs

if [ -z "$1" ]; then
    echo "ERROR: stack ID must be first argument passed in"
    exit 1
fi

STACK_ID=$1 

echo "Creating SSH config"
testenv stack show ssh-config "${STACK_ID}" >  /tmp/ssh-config-"${STACK_ID}".conf
echo "Creating symbols file"
testenv stack show symbols "${STACK_ID}" > /tmp/symbols-"${STACK_ID}".json

echo "Creating ansible inventory file"
# shellcheck disable=SC2002
CONTROLPLANE_IPS=$(cat "/tmp/symbols-${STACK_ID}.json" | jq -r '.control_host_ips | .[] ' | tr '\n' ' ' | sed 's/ $/\n/')
export CONTROLPLANE_IPS

# shellcheck disable=SC2002
DATAPATH_IPS=$(cat "/tmp/symbols-${STACK_ID}.json" | jq -r '.datapath_host_ips | .[] ' | tr '\n' ' ' | sed 's/ $/\n/')
export DATAPATH_IPS

export F="/tmp/inventory-${STACK_ID}.ini"
rm -f "${F}"

echo "[controlplane]" >> "${F}"
for ip in $CONTROLPLANE_IPS;
do
    # shellcheck disable=SC2002
    CTRL_USER=$(cat "/tmp/symbols-${STACK_ID}.json" | jq -r '.control_host_ssh_username')
    # echo "$ip ansible_user=${CTRL_USER} ansible_ssh_private_key_file=/Users/R.Johnston/.ssh/id_rsa.pub" >> "${F}"
    echo "$ip ansible_user=${CTRL_USER}" >> "${F}"
done

echo "[dataplane]" >> "${F}"
for ip in $DATAPATH_IPS;
do
    # shellcheck disable=SC2002
    DATA_USER=$(cat "/tmp/symbols-${STACK_ID}.json" | jq -r '.datapath_host_ssh_username')
    # echo "$ip ansible_user=${DATA_USER} ansible_ssh_private_key_file=/Users/R.Johnston/.ssh/id_rsa.pub" >> "${F}"
    echo "$ip ansible_user=${DATA_USER}" >> "${F}"
done
echo "  -> wrote to ${F}"

echo "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook upg-setup-playbook.yaml -i ${F}"
echo "rm ~/.testenv-ssh-config.conf && ln -s /tmp/ssh-config-${STACK_ID}.conf ~/.testenv-ssh-config.conf"
echo "rm ~/.testenv-symbols.json && ln -s /tmp/symbols-${STACK_ID}.json ~/.testenv-symbols.json"
echo "export TESTRUN_SYMBOLS=/tmp/symbols-${STACK_ID}.json"
echo "export TESTRUN_SSHCFG=/tmp/ssh-config-${STACK_ID}.conf"
echo "export TESTRUN_SSHKEY=~/.ssh/id_rsa.pub"





