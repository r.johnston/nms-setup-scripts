#!/usr/bin/env bash

set -eo pipefail

#################################
#
# NGINX NMS NAP Dependencies Support Script for EL7 & EL8
#
# Copyright (C) F5, Inc; 2022
#################################
#
# Description
#
# Script should be run as sudo user
#
# Script is intended to run on EL7/EL8 OS systems
#  to prepare the system for installation of NMS NAP module.
# It will install all needed Perl dependencies needed
#
# To successfully run this script you should have
# active NGINX-Plus subscription, to access NGINX-Plus EPEL
# repository with published Perl dependencies for EL7/EL8
#
# Script needs you to have valid NGINX-Plus access keys located in:
# /etc/ssl/nginx/nginx-plus.crt
# /etc/ssl/nginx/nginx-plus.key
#
# If the NGINX-Plus files are located in other place / with other name - you''ll
# need to provide correct paths as arguments:
# ${1}: path to .crt file
# ${2}: path to .key file
#
# Support: support@f5.com
#################################

check_os(){
  if [[ "${1}" == "na" ]]; then
    pr_info_err "OS Version : ${1}; Supported OS versions: el7; el8; Exiting"
    exit 1
  else
    pr_info "Detected OS version: ${1}"
  fi
}

check_key(){
  if [[ ! -f "${1}" ]]; then
    pr_info_err "file ${1} wasn't found"
    if [[ "${1}" == *".crt" ]]; then
      pr_info "please, provide valid path to ${1} as \${1} arg"
      export err_crt_flag="1"
    elif [[ "${1}" == *".key" ]]; then
      pr_info "please, provide valid path to ${1} as \${2} arg"
      export err_key_flag="1"
    fi
  fi
}

set_key_values() {
  err_crt_flag="0"
  err_key_flag="0"

  if [[ "${1}" == "def_crt" ]]; then
    export nginx_plus_crt="/etc/ssl/nginx/nginx-plus.crt"
  else
    export nginx_plus_crt="${1}"
  fi
  check_key "${nginx_plus_crt}"
  if [[ "${2}" == "def_key" ]]; then
    export nginx_plus_key="/etc/ssl/nginx/nginx-plus.key"
  else
    export nginx_plus_key="${2}"
  fi
  check_key "${nginx_plus_key}"

  if [[ "${err_crt_flag}" != "1" ]]; then
    pr_info " NGINX Plus repo: .crt file set: ${nginx_plus_crt}"
  fi
  if [[ "${err_key_flag}" != "1" ]]; then
    pr_info " NGINX Plus repo: .key file set: ${nginx_plus_key}"
  fi

  if [[ "${err_crt_flag}" == "1" ]] || [[ "${err_key_flag}" == "1" ]]; then
    pr_info_err "Errors found, scheck the log above"
    exit 1
  fi
}

install_ext_dependency() {
  pr_info "---> Installing: ${1} from ${2}"
  set +e
  curl "${2}" > /tmp/pkg.rpm && sudo yum install -y /tmp/pkg.rpm && sudo rm /tmp/pkg.rpm
  set -eo pipefail
}

pr_info_start() {
  echo "--- NGINX NMS NAP: EL7/EL8 Support script ---"
}

pr_info_h1() {
  echo "--> ${1}"
}

pr_info_err() {
  echo "!ERROR!: ${1}"
}

pr_info() {
  echo "- ${1}"
}

setup_nginx_epel_repo(){
  pr_info_h1 "Add NGINX EPEL ${1} repo to /etc/yum.repos.d/nginx-epel-${1}.repo"
  if [[ -f "/etc/yum.repos.d/nginx-epel-${1}.repo" ]]; then
    pr_info " rm /etc/yum.repos.d/nginx-epel-${1}.repo"
    rm "/etc/yum.repos.d/nginx-epel-${1}.repo"
  fi

  os_number=$(echo "${1}" | tr -d el)
	cat <<- EOF > "/etc/yum.repos.d/nginx-epel-${1}.repo"
	[nginx-epel-${1}]
	name=nginx epel ${1} repo
	baseurl=https://pkgs.nginx.com/epel/${os_number}/x86_64/
	sslclientcert=${nginx_plus_crt}
	sslclientkey=${nginx_plus_key}
	gpgcheck=0
	enabled=1
	EOF

	pr_info " contents of /etc/yum.repos.d/nginx-epel-${1}.repo:"
  cat "/etc/yum.repos.d/nginx-epel-${1}.repo"
}

remove_nginx_epel_repo(){
  pr_info_h1 "removing NGINX EPEL ${1} repo from the system"
  rm "/etc/yum.repos.d/nginx-epel-${1}.repo"
}

el8_setup() {
  pr_info_h1 "Install needed perl packages from Official CentOS repo vault.centos.org"
  install_ext_dependency "perl-MIME-Types" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-MIME-Types-2.17-3.el8.noarch.rpm"
  install_ext_dependency "perl-IO-stringy" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-IO-stringy-2.111-9.el8.noarch.rpm"

  pr_info_h1 "install Perl dependencies from NGINX EPEL EL8 repo"
  yum install -y perl-Config-IniFiles perl-IPC-ShareLite perl-Parse-RecDescent perl-SOAP-Lite perl-Text-CSV_XS perl-Text-Iconv perl-thrift

  pr_info_h1 "Install rest needed perl packages from Official CentOS repo vault.centos.org"
  install_ext_dependency "perl-common-sense" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-common-sense-3.7.4-8.el8.x86_64.rpm"
  install_ext_dependency "perl-Types-Serialiser" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-Types-Serialiser-1.0-12.el8.noarch.rpm"
  install_ext_dependency "perl-JSON-XS" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-JSON-XS-3.04-3.el8.x86_64.rpm"
  install_ext_dependency "perl-Class-Accessor" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-Class-Accessor-0.51-2.el8.noarch.rpm"
  install_ext_dependency "perl-Clone" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-Clone-0.39-5.el8.x86_64.rpm"
  install_ext_dependency "perl-HTML-Tree" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-HTML-Tree-5.07-2.el8.noarch.rpm"
  install_ext_dependency "perl-XML-Twig" "https://vault.centos.org/centos/8/PowerTools/x86_64/os/Packages/perl-XML-Twig-3.52-7.el8.noarch.rpm"

  pr_info_h1 "Install rest needed perl packages from Official Fedora Project repo *.fedoraproject.org"
  install_ext_dependency "re2" "https://download-ib01.fedoraproject.org/pub/epel/8/Everything/x86_64/Packages/r/re2-20190801-1.el8.x86_64.rpm"
}

el7_setup() {
  pr_info_h1 "Install needed perl packages from Official CentOS repo vault.centos.org"
  install_ext_dependency "perl-Data-Dumper" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-Data-Dumper-2.145-3.el7.x86_64.rpm"
  install_ext_dependency "perl-IO-String" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-IO-String-1.08-19.el7.noarch.rpm"
  install_ext_dependency "perl-Class-Inspector" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-Class-Inspector-1.28-2.el7.noarch.rpm"

  pr_info_h1 "install Perl dependencies from NGINX EPEL EL8 repo"
  yum install -y perl-Config-IniFiles perl-IPC-ShareLite perl-SOAP-Lite perl-thrift

  pr_info_h1 "Install rest needed perl packages from Official CentOS repo vault.centos.org"
  install_ext_dependency "perl-Parse-RecDescent" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-Parse-RecDescent-1.967009-5.el7.noarch.rpm"
  install_ext_dependency "perl-Clone" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-Clone-0.34-5.el7.x86_64.rpm"
  install_ext_dependency "perl-Text-CSV_XS" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-Text-CSV_XS-1.00-3.el7.x86_64.rpm"
  install_ext_dependency "perl-Text-Iconv" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-Text-Iconv-1.7-18.el7.x86_64.rpm"
  install_ext_dependency "perl-XML-LibXML" "http://mirror.centos.org/centos/7/os/x86_64/Packages/perl-XML-LibXML-2.0018-5.el7.x86_64.rpm"

  pr_info_h1 "Install rest needed perl packages from Official Fedora Project repo *.fedoraproject.org"
  install_ext_dependency "perl-JSON-XS" "https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/p/perl-JSON-XS-3.01-2.el7.x86_64.rpm"
  install_ext_dependency "re2" "https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/r/re2-20160401-2.el7.x86_64.rpm"
  install_ext_dependency "perl-XML-Hash-LX" "https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/p/perl-XML-Hash-LX-0.60.300-1.el7.noarch.rpm"
  install_ext_dependency "perl-Class-Accessor" "https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/p/perl-Class-Accessor-0.34-12.el7.noarch.rpm"
  install_ext_dependency "perl-ZMQ-Constants" "https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/p/perl-ZMQ-Constants-1.04-1.el7.noarch.rpm"
  install_ext_dependency "perl-Data-MessagePack" "https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/p/perl-Data-MessagePack-0.48-1.el7.x86_64.rpm"
}

main() {
  pr_info_start
  check_os "${1}"
  set_key_values "${2}" "${3}"
  setup_nginx_epel_repo "${1}"

  if [[ "${1}" == "el7" ]]; then
    el7_setup
  elif [[ "${1}" == "el8" ]]; then
    el8_setup
  fi

  remove_nginx_epel_repo "${1}"
}

if [[ -z ${1} ]]; then
  nginx_plus_crt='def_crt'
else
  nginx_plus_crt="${1}"
fi
if [[ -z ${2} ]]; then
  nginx_plus_key='def_key'
else
  nginx_plus_key="${2}"
fi

if [[ $(hostnamectl | grep "el7") != "" ]]; then
  os_version="el7"
elif [[ $(hostnamectl | grep "el8") != "" ]]; then
  os_version="el8"
else
  os_version="na"
fi

main "${os_version}" "${nginx_plus_crt}" "${nginx_plus_key}"